﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using win = System.Windows;
using Metrics;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Wpf;
using OxyPlot.Annotations;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace PlanComparisonV13
{
    public class MainViewModel
    {
        #region Properties
        public IEnumerable<Structure> Structures { get; private set; }
        public IEnumerable<PlanSetup> PlansInScope { get; private set; }
        public IEnumerable<PlanName> PlanCollection { get; set; }
        public Patient patient { get; }
        public Image image { get; }
        public PlotModel PlotModel { get; private set; }
        public string ChosenStructureId { get; set; }
        public string ChosenSecondStructureId { get; set; }
        public string ChosenPlotType { get; set; }
        public double NumericInput_1 { get; set; }
        public double NumericInput_2 { get; set; }
        public double NumericInput_3 { get; set; }
        public double NumericInput_4 { get; set; }
        public double NumericInput_5 { get; set; }
        public string AbsRelDose { get; set; }
        public string AbsRelVol { get; set; }

        public string Direction { get; set; }
        #endregion

        // Constructor
        public MainViewModel(ScriptContext context)
        {
            // Set properties
            PlansInScope = GetPlansInScope(context);
            Structures = GetPlanStructures(context);
            PlanCollection = GetPlanCollection(PlansInScope);
            PlotModel = CreatePlotModel();
            image = context.Image;
            patient = context.Patient;

            // Enable creation of structures etc.
            patient.BeginModifications();
        }

        #region Private methods for setting properties
        // Get a list of all plans in the current context
        private IEnumerable<PlanSetup> GetPlansInScope(ScriptContext context)
        {
            var plans = context.PlansInScope != null
                ? context.PlansInScope : null;

            return plans.Where(p => p.IsDoseValid);
        }

        // Get a list of all structures in the currently loaded structure set
        private IEnumerable<Structure> GetPlanStructures(ScriptContext context)
        {
            var plan = context.PlanSetup != null ? context.PlanSetup : null;
            return plan.StructureSet != null
                ? plan.StructureSet.Structures : null;
        }

        // Make list of PlanName objects. The PlanName class implements INotifyPropertyChanged. 
        // This is so that the script can tell if a plan is checked or unchecked. 
        // Used to uncheck all plans when switching between plot views.
        private IEnumerable<PlanName> GetPlanCollection(IEnumerable<PlanSetup> plansInScope)
        {
            var List = new List<PlanName>();
            foreach (var plan in plansInScope)
            {
                var planName = new PlanName() { Name = plan.Id };
                List.Add(planName);
            }
            return List;
        }

        // Create an empty PlotModel
        private PlotModel CreatePlotModel()
        {
            PlotModel plotModel = new PlotModel();
            return plotModel;
        }
        #endregion

        #region Private methods for manipulating the PlotModel

        // Find a plan series in the PlotModel
        private OxyPlot.Series.Series FindSeries(string id)
        {
            return PlotModel.Series.FirstOrDefault(x =>
            x.Tag.ToString().Contains(id));
        }

        // Find an annotation in the PlotModel (line or text in the plot)
        private OxyPlot.Annotations.Annotation FindAnnotation(string id)
        {
            return PlotModel.Annotations.FirstOrDefault(x =>
            (string)x.Tag == id);
        }

        // Find index of plan in PlansInScope, used for placement of boxes and bars on the x-axis
        private double PlanIndex(PlanSetup plan)
        {
            List<PlanSetup> PlanList = PlansInScope.ToList();
            string id = plan.Id;

            return (double)PlanList.FindIndex(pln => pln.Id == id);
        }

        // Update plot when data is added or removed
        private void UpdatePlot()
        {
            PlotModel.InvalidatePlot(true);
        }
        #endregion

        #region Public methods for manipulating the plot or extracting data

        // Set up the correct axes according to the chosen plot type
        public void ShowPlot()
        {
            IEnumerable<string> planIDs = PlansInScope.Select(p => p.Id);

            switch (ChosenPlotType)
            {
                case "Empty":
                    PlotModel.Title = "Choose a plot to display:";
                    break;

                case "Scatter: Conformity, Homogeneity, Gradient":
                    PlotViews.ScatterPlotThreeYaxes CHGScatterPlot = new PlotViews.ScatterPlotThreeYaxes(); 
                    CHGScatterPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "Plan ID", new string[] { "CI", "HI", "GI" }, planIDs);
                    CHGScatterPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    CHGScatterPlot.SetTitle(PlotModel, "Conformity, Homogeneity, and Gradient indices");
                    break;

                case "Scatter: Target cold volume size and distance from edge":
                    PlotViews.ScatterPlot ColdScatter = new PlotViews.ScatterPlot();
                    ColdScatter.SetAxes(PlotModel, 0, 0, 0, 0, "Distance from target edge [mm]", new string[] { "Size [cc]" }, null);
                    ColdScatter.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    ColdScatter.SetTitle(PlotModel, "Size and COM location of contiguous cold volumes");
                    break;

                case "Scatter: Structure hot volume size and distance from structure edge":
                    PlotViews.ScatterPlot HotScatter = new PlotViews.ScatterPlot();
                    HotScatter.SetAxes(PlotModel, 0, 0, 0, 0, "Distance from target edge [mm]", new string[] { "Size [cc]" }, null);
                    HotScatter.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    HotScatter.SetTitle(PlotModel, "Size and COM location of contiguous hot volumes");
                    break;

                case "Scatter: Mean structure dose for all uncertainty scenarios":
                    PlotViews.CategoryScatterPlot meanBoxPlot = new PlotViews.CategoryScatterPlot();
                    meanBoxPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "PlanID", new string[] { "Dose [Gy]" }, planIDs);
                    meanBoxPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    meanBoxPlot.SetTitle(PlotModel, "Mean dose in all uncertainty scnarios");
                    break;

                case "Scatter: D(V) for all uncertainty scenarios":
                    PlotViews.CategoryScatterPlot dVBoxPlot = new PlotViews.CategoryScatterPlot();
                    dVBoxPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "PlanID", new string[] { "Dose [Gy]" }, planIDs);
                    dVBoxPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    dVBoxPlot.SetTitle(PlotModel, "D(V) in all uncertainty scnarios");
                    break;

                case "Scatter: V(D) for all uncertainty scenarios":
                    PlotViews.CategoryScatterPlot vDBoxPlot = new PlotViews.CategoryScatterPlot();
                    vDBoxPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "PlanID", new string[] { "Volume [cc]" }, planIDs);
                    vDBoxPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    vDBoxPlot.SetTitle(PlotModel, "V(D) in all uncertainty scnarios");
                    break;

                case "Scatter: OAR gradient":
                    PlotViews.ScatterPlot OARGradientCurve = new PlotViews.ScatterPlot();
                    OARGradientCurve.SetAxes(PlotModel, 0, 0, 0, 0, "Dose [Gy]", new string[] { "Distance [mm]" }, null);
                    OARGradientCurve.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    OARGradientCurve.SetTitle(PlotModel, "Distance OAR to isodose");
                    break;

                case "Scatter: Directional dose gradient as function of dose (direction)":
                    PlotViews.ScatterPlot DirGradPlot2 = new PlotViews.ScatterPlot();
                    DirGradPlot2.SetAxes(PlotModel, 0, 0, 0, 0, "Dose [Gy]", new string[] { "Distance to isodose [mm]" }, null);
                    DirGradPlot2.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    DirGradPlot2.SetTitle(PlotModel, "Distance to range of isodoses in chosen direction");
                    break;

                case "Scatter: Directional dose gradient as function of dose (towards structure COM)":
                    PlotViews.ScatterPlot DirGradPlot3 = new PlotViews.ScatterPlot();
                    DirGradPlot3.SetAxes(PlotModel, 0, 0, 0, 0, "Dose [Gy]", new string[] { "Distance to isodose [mm]" }, null);
                    DirGradPlot3.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    DirGradPlot3.SetTitle(PlotModel, "Distance to range of isodoses in chosen direction");
                    break;

                case "Bar: Conformity index at various dose levels":
                    PlotViews.BarPlot conformityBarPlot = new PlotViews.BarPlot();
                    conformityBarPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "Plan ID", new string[] { "CI at " + NumericInput_1 + " Gy", "CI at " + NumericInput_2 + " Gy", "CI at " + NumericInput_3 + " Gy" }, planIDs);
                    conformityBarPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    conformityBarPlot.SetTitle(PlotModel, "Conformityindex at three dose levels");
                    break;

                case "Bar: Cold spot sizes":
                    PlotViews.BarPlot ColdSizeBar = new PlotViews.BarPlot();
                    ColdSizeBar.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, PlansInScope.Count() - 0.5, 0, "Plan ID", new string[] { "Volume [cc]", "Large cold spot penalty" }, planIDs);
                    ColdSizeBar.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    ColdSizeBar.SetTitle(PlotModel, "Size of contiguous cold spots");
                    break;

                case "Bar: Cold spot sizes in PlanUncertainties":
                    PlotViews.BarPlot UPlnColdSpotBar = new PlotViews.BarPlot();
                    UPlnColdSpotBar.SetAxes(PlotModel, -0.5, GetNumberOfUplns(PlansInScope), 0, 0, "PlanUncertainty ID", new string[] { "Size [cc]" }, null);
                    UPlnColdSpotBar.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    UPlnColdSpotBar.SetTitle(PlotModel, "Size of contiguous cold spots in each plan uncertainty scenario");
                    break;

                case "Bar: Hot spot sizes":
                    PlotViews.BarPlot HotSizeBar = new PlotViews.BarPlot();
                    HotSizeBar.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "Plan ID", new string[] { "Volume [cc]", "Large hot spot penalty" }, planIDs);
                    HotSizeBar.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    HotSizeBar.SetTitle(PlotModel, "Size of contiguous hot spots");
                    break;

                case "Bar: Hot spot sizes in PlanUncertainties":
                    PlotViews.BarPlot UPlnHotSpotBar = new PlotViews.BarPlot();
                    UPlnHotSpotBar.SetAxes(PlotModel, -0.5, GetNumberOfUplns(PlansInScope), 0, 0, "PlanUncertainty ID", new string[] { "Size [cc]" }, null);
                    UPlnHotSpotBar.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    UPlnHotSpotBar.SetTitle(PlotModel, "Size of contiguous cold spots in each plan uncertainty scenario");
                    break;

                case "Bar: Beam path lengths through ROI":
                    PlotViews.BarPlot BeamPathPlot = new PlotViews.BarPlot();
                    BeamPathPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "Plan ID", new string[] { "Healthy brain vol in beam path [cc]" }, planIDs);
                    BeamPathPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    BeamPathPlot.SetTitle(PlotModel, "Beam path through chosen structure");
                    break;

                case "Curve: Cumulative Gradient Index":
                    PlotViews.LinePlot CGIPlot = new PlotViews.LinePlot();
                    CGIPlot.SetAxes(PlotModel, 0, 0, 0, 0, "Dose level [Gy]", new string[] { "Distance(X Gy to Dref)" }, null);
                    CGIPlot.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    CGIPlot.SetTitle(PlotModel, "Cumulative dose gradient index histogram curve");
                    break;

                case "Curve: Cumulative Gradient Index (Reff)":
                    PlotViews.LinePlot gradientMayoPlot = new PlotViews.LinePlot();
                    gradientMayoPlot.SetAxes(PlotModel, 0, 0, 0, 0, "Dose level [Gy]", new string[] { "Reff(X Gy) - Reff(Dref)" }, null);
                    gradientMayoPlot.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    gradientMayoPlot.SetTitle(PlotModel, "Cumulative dose gradient index histogram curve");
                    break;

                case "Curve: Voxelwise minimum dose - cumulative DVH":
                    PlotViews.LinePlot voxelwiseMinHistogramPlot = new PlotViews.LinePlot();
                    voxelwiseMinHistogramPlot.SetAxes(PlotModel, 0, 0, 0, 0, "Dose [gy]", new string[] { "Volume [cc]" }, null);
                    voxelwiseMinHistogramPlot.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    voxelwiseMinHistogramPlot.SetTitle(PlotModel, "Voxelwise minimum histogram");
                    break;

                case "Curve: Dose profile between two structures":
                    PlotViews.LinePlot ProfilePlot = new PlotViews.LinePlot();
                    ProfilePlot.SetAxes(PlotModel, 0, 0, 0, 0, "Location [mm]", new string[] { "Dose [Gy]" }, null);
                    ProfilePlot.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    ProfilePlot.SetTitle(PlotModel, "Dose profile between two COMs");
                    break;

                case "Curve: Directional dose gradient as function of CC position (direction)":
                    PlotViews.LinePlot DirGradPlot = new PlotViews.LinePlot();
                    DirGradPlot.SetAxes(PlotModel, 0, 0, 0, 0, "Distance [mm]", new string[] { "Position along CC axis [mm]" }, null);
                    DirGradPlot.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    DirGradPlot.SetTitle(PlotModel, "Distance to chosen isodose in chosen direction");
                    break;

                case "Curve: Directional dose gradient as function of CC position (towards structure COM)":
                    PlotViews.LinePlot DirGradPlot1 = new PlotViews.LinePlot();
                    DirGradPlot1.SetAxes(PlotModel, 0, 0, 0, 0, "Distance [mm]", new string[] { "Position along CC axis [mm]" }, null);
                    DirGradPlot1.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    DirGradPlot1.SetTitle(PlotModel, "Distance to chosen isodose in chosen direction");
                    break;

                case "Overlap volume histogram: Target cold volume":
                    PlotViews.LinePlot ColdOVH = new PlotViews.LinePlot();
                    ColdOVH.SetAxes(PlotModel, 0, 0, 0, 0, "Distance [mm]", new string[] { "Volume [cc]" }, null);
                    ColdOVH.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    ColdOVH.SetTitle(PlotModel, "Target cold dose-location histogram");
                    break;

                case "Overlap volume histogram: Structure hot volume":
                    PlotViews.LinePlot HotOVH = new PlotViews.LinePlot();
                    HotOVH.SetAxes(PlotModel, 0, 0, 0, 0, "Distance [mm]", new string[] { "Volume [cc]" }, null);
                    HotOVH.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    HotOVH.SetTitle(PlotModel, "Structure hot OVH");
                    break;

                case "Histogram: Voxelwise dose error bar width":
                    PlotViews.LineBarPlot doseErrorBarPlot = new PlotViews.LineBarPlot();
                    doseErrorBarPlot.SetAxes(PlotModel, 0, 0, 0, 0, "Dose deviation from nominal [Gy]", new string[] { "Volume [cc]" }, null);
                    doseErrorBarPlot.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    doseErrorBarPlot.SetTitle(PlotModel, "Voxelwise dose error bar width");
                    break;

                case "Histogram: Voxelwise maximum dose deviation":
                    PlotViews.LineBarPlot maxDevPlot = new PlotViews.LineBarPlot();
                    maxDevPlot.SetAxes(PlotModel, 0, 0, 0, 0, "Dose deviation from nominal [Gy]", new string[] { "Volume [cc]" }, null);
                    maxDevPlot.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    maxDevPlot.SetTitle(PlotModel, "Maximum voxelwise dose deviation");
                    break;

                case "Histogram: Differential dose gradient index":
                    PlotViews.LineBarPlot difGCIPlot = new PlotViews.LineBarPlot();
                    difGCIPlot.SetAxes(PlotModel, 0, 0, 0, 0, "Dose [Gy]", new string[] { "GCI [mm]" }, null);
                    difGCIPlot.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    difGCIPlot.SetTitle(PlotModel, "Differential gradient curve index");
                    break;

                case "Area: Spatial DVH":
                    PlotViews.LinePlot SpatialDVHPlot = new PlotViews.LinePlot();
                    SpatialDVHPlot.SetAxes(PlotModel, 0, 0, 0, 0, "Dose [Gy]", new string[] { "Volume [cc]" }, null);
                    SpatialDVHPlot.SetLegend(PlotModel, true, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    SpatialDVHPlot.SetTitle(PlotModel, "Spatial DVH for the chosen structure and shell distances.");
                    break;

                case "Test: Dose map around OAR":
                    PlotViews.HeatMapPlot OARGradHeatMap = new PlotViews.HeatMapPlot();
                    OARGradHeatMap.SetAxes(PlotModel, 0, 0, 0, 0, "Phi [deg]", new string[] { "Theta [deg]" }, null);
                    OARGradHeatMap.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    OARGradHeatMap.SetTitle(PlotModel, "Dose at X cm distance from OAR");
                    break;

                case "Test: Distance to dose":
                    PlotViews.HeatMapPlot OARGradHeatMap1 = new PlotViews.HeatMapPlot();
                    OARGradHeatMap1.SetAxes(PlotModel, 0, 0, 0, 0, "Phi [deg]", new string[] { "Theta [deg]" }, null);
                    OARGradHeatMap1.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    OARGradHeatMap1.SetTitle(PlotModel, "Distance to X Gy isodose");
                    break;
            }
        }

        // Add series when a plan is checked
        public void AddPlanItem(PlanSetup plan)
        {
            // If no structure is chosen
            if (ChosenStructureId == null)
            {
                win.MessageBox.Show("Please choose a structure for metric evaluation!");
                return;
            }
            // If the checked plan does not have the chosen structure in its structure set:
            else if (!plan.StructureSet.Structures.Any(str => (str.Id == ChosenStructureId & str.IsEmpty == false)))
            {
                win.MessageBox.Show("The plan is not connected to a structure with the chosen ID, or the structure has no segment.");
                return;
            }
            // If no dose is calculated
            else if (plan.Dose == null)
            {
                win.MessageBox.Show("The plan has no calculated dose!");
                return;
            }
            // If everything is OK add the correct series type to the plot
            else
            {
                // Ensure dose is given in Gy
                plan.DoseValuePresentation = DoseValuePresentation.Absolute;

                Structure structure = plan.StructureSet.Structures.First(s => s.Id == ChosenStructureId);

                // If structure is very large, request affirmation before starting the calculation. This is to prevent accidental calculations of complex metrics on e.g. BODY, which can take a LONG time.
                if (structure.Volume > 200)
                {
                    var result = win.MessageBox.Show(string.Format("The volume of the chosen structure is {0}cc. Some calculations may take a long time for large structures. Do you want to continue?", structure.Volume), "Continue?", win.MessageBoxButton.YesNo);

                    if (result == win.MessageBoxResult.No)
                    {
                        return;
                    }
                }

                // Add the requested series to the plotmodel and update the view
                switch (ChosenPlotType)
                {
                    case "Scatter: Conformity, Homogeneity, Gradient":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }

                        PlotModel.Series.Add(CreateHorizontalLine(0, "GI", "Linear", plan.Id));
                        PlotModel.Series.Add(CreateHorizontalLine(0, "HI", "Linear", plan.Id));
                        PlotModel.Series.Add(CreateHorizontalLine(1, "CI", "Linear", plan.Id));
                        PlotModel.Series.Add(CreateCIScatterSeries(plan, structure));
                        PlotModel.Series.Add(CreateHIScatterSeries(plan, structure));
                        PlotModel.Series.Add(CreateGIReffScatterSeries(plan));
                        break;

                    case "Scatter: Target cold volume size and distance from edge":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        CreateColdSpotLocationScatterSeries(plan, structure);
                        break;

                    case "Scatter: Structure hot volume size and distance from structure edge":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        CreateHotSpotLocationScatterSeries(plan, structure);
                        break;

                    case "Scatter: Mean structure dose for all uncertainty scenarios":
                        AddMeanDoseScatterSeries(plan, structure);
                        break;

                    case "Scatter: D(V) for all uncertainty scenarios":
                        AddDxDoseScatterSeries(plan, structure, (AbsRelVol == "Absolute (cc)"));
                        break;

                    case "Scatter: V(D) for all uncertainty scenarios":
                        AddVxScatterSeries(plan, structure, (AbsRelDose == "Absolute (Gy)"));
                        break;

                    case "Scatter: OAR gradient":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        AddOARGradientSeries(plan, structure); // user input = x% of closest points that are shown in the plot
                        break;

                    case "Scatter: Directional dose gradient as function of dose (direction)":
                        //AddDirGradLineSeriesCC(plan, structure, Direction, NumericInput_1);
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        AddDirGradScatterSeriesCCAvg(plan, structure);
                        break;

                    case "Scatter: Directional dose gradient as function of dose (towards structure COM)":
                        // AddDirGradOARSeries(plan, structure, ChosenSecondStructureId, NumericInput_1);
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        try
                        {
                            Structure secondStructure = GetStructure(plan, ChosenSecondStructureId);
                            AddDirGradScatterSeriesCCAvg(plan, structure, secondStructure);
                        }
                        catch
                        {
                            win.MessageBox.Show("Choose a valid second structure!");
                        }
                        break;

                    case "Bar: Conformity index at various dose levels":
                        AddThreeConfSeries(plan, structure);
                        break;

                    case "Bar: Cold spot sizes":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        AddColdSpotSizeColumnSeries(plan, structure);
                        break;

                    case "Bar: Cold spot sizes in PlanUncertainties":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        AddStackedBarColdSpotSeries(plan, structure);
                        break;

                    case "Bar: Hot spot sizes":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        AddHotSpotSizeColumnSeries(plan, structure);
                        break;

                    case "Bar: Hot spot sizes in PlanUncertainties":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        AddStackedBarHotSpotSeries(plan, structure); 
                        break;

                    case "Bar: Beam path lengths through ROI":
                        CreateBeamPathPlot(plan, structure);
                        break;

                    case "Curve: Cumulative Gradient Index":
                        PlotModel.Series.Add(CreateCumulativeGradientCurveSeries(plan, structure));
                        break;

                    case "Curve: Cumulative Gradient Index (Reff)":
                        PlotModel.Series.Add(CreateGradientEffRadCurveSeries(plan));
                        break;

                    case "Curve: Voxelwise minimum dose - cumulative DVH":
                        PlotModel.Series.Add(CreateVoxelwiseMinHistogramSeries(plan, structure));
                        break;

                    case "Curve: Dose profile between two structures":
                        PlotModel.Series.Add(CreateDoseProfileLineSeries(plan, structure));
                        break;

                    case "Curve: Directional dose gradient as function of CC position (direction)":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        AddDirGradLineSeriesCC(plan, structure);
                        break;

                    case "Curve: Directional dose gradient as function of CC position (towards structure COM)":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        try
                        {
                            Structure secondStructure = plan.StructureSet.Structures.First(s => s.Id == ChosenSecondStructureId);
                            AddDirGradLineSeriesCC(plan, structure, secondStructure);
                        }
                        catch
                        {
                            win.MessageBox.Show("Chosen structure ID cannot be found in the plan's structure set!");
                            return;
                        }
                        break;

                    case "Overlap volume histogram: Target cold volume":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        PlotModel.Series.Add(CreateCumulativeColdVolumeOVH(plan, structure));
                        break;

                    case "Overlap volume histogram: Structure hot volume":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation dose level!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        try
                        {
                            Structure structure2 = plan.StructureSet.Structures.First(s => s.Id == ChosenSecondStructureId);
                            PlotModel.Series.Add(CreateCumulativeHotVolumeOVH(plan, structure, structure2));
                        }
                        catch
                        {
                            win.MessageBox.Show("The plan's structure set does not contain a structure with the given ID!");
                        }
                        break;

                    case "Histogram: Voxelwise dose error bar width":
                        PlotModel.Series.Add(CreateErrorBarWidthSeries(plan, structure));
                        break;

                    case "Histogram: Voxelwise maximum dose deviation":
                        PlotModel.Series.Add(CreateMaxDeviationSeries(plan, structure));
                        break;

                    case "Histogram: Differential dose gradient index":
                        PlotModel.Series.Add(CreateDifferentialGCISeries(plan));
                        break;

                    case "Area: Spatial DVH":
                        CreateSpatialDVHPlot(plan, structure);
                        break;

                    case "Test: Dose map around OAR":
                        // A dose level must be given. Default value 0 may cause confusion or long calculation times.
                        if (NumericInput_1 == 0)
                        {
                            win.MessageBox.Show("Please enter a valid evaluation distance larger than 0!");

                            // Uncheck all plan boxes. Otherwise the box that was just checked will remain checked.
                            UncheckAllBoxes();

                            return;
                        }
                        CreateDoseAtDistanceHeatMap(plan, structure);
                        break;
                }

                UpdatePlot();
            }
        }

        // Remove series from plot when box is unchecked
        public void RemovePlanItem(PlanSetup plan)
        {
            while (PlotModel.Series.Any(ser => (string)ser.Tag == plan.Id))
            {
                var series = FindSeries(plan.Id);
                PlotModel.Series.Remove(series);
            }
            while (PlotModel.Annotations.Any(ser => (string)ser.Tag == plan.Id))
            {
                var annotation = FindAnnotation(plan.Id);
                PlotModel.Annotations.Remove(annotation);
            }
            UpdatePlot();
        }

        // Clear previous plot model
        public void OnClear()
        {
            PlotModel.Axes.Clear();
            PlotModel.Series.Clear();
            PlotModel.InvalidatePlot(true);
        }

        // Clear all plot series when input is changed
        public void RemoveAllSeries()
        {
            PlotModel.Series.Clear();
            PlotModel.InvalidatePlot(true);
        }

        // Uncheck all checkboxes
        public void UncheckAllBoxes()
        {
            foreach (var pn in PlanCollection)
            {
                pn.IsChecked = false;
            }
        }

        // Export plot as pdf
        public void ExportPlotAsPdf(string filePath)
        {
            using (var stream = File.Create(filePath))
            {
                PdfExporter.Export(PlotModel, stream, 600, 400);
            }
        }

        // Export plotted data as txt file
        public void ExportDataAsTxt(string filePath)
        {
            using (var sw = new StreamWriter(filePath))
            {
                foreach (var ser in PlotModel.Series)
                {
                    if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.LineSeries().GetType()) | OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.LinearBarSeries().GetType()) | OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.AreaSeries().GetType()))
                    {
                        var dataSer = (OxyPlot.Series.DataPointSeries)ser;
                        sw.WriteLine(string.Format("#{0}", ser.Title));
                        sw.WriteLine(string.Format("#{0}\t{1}", PlotModel.Axes.FirstOrDefault(ax => ax.Key == dataSer.XAxisKey).Title, PlotModel.Axes.FirstOrDefault(ax => ax.Key == dataSer.YAxisKey).Title));

                        foreach (var point in dataSer.Points)
                        {
                            sw.WriteLine(string.Format("{0}\t{1}", point.X.ToString(), point.Y.ToString()));
                        }
                        sw.WriteLine(string.Format("\n\n"));
                    }

                    else if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.ScatterSeries().GetType()))
                    {
                        var dataSer = (OxyPlot.Series.ScatterSeries)ser;
                        sw.WriteLine(string.Format("#{0}", ser.Title));
                        sw.WriteLine(string.Format("#{0}\t{1}", PlotModel.Axes.FirstOrDefault(ax => ax.Key == dataSer.XAxisKey).Title, PlotModel.Axes.FirstOrDefault(ax => ax.Key == dataSer.YAxisKey).Title));

                        foreach (var point in dataSer.Points)
                        {
                            sw.WriteLine(string.Format("{0}\t{1}", point.X.ToString(), point.Y.ToString()));
                        }
                        sw.WriteLine(string.Format("\n\n"));
                    }

                    else if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.ColumnSeries().GetType()))
                    {
                        OxyPlot.Axes.CategoryAxis xaxis = (OxyPlot.Axes.CategoryAxis)PlotModel.Axes.First(ax => ax.GetType() == new OxyPlot.Axes.CategoryAxis().GetType()); // get category x axis
                        var dataSer = (OxyPlot.Series.ColumnSeries)ser;
                        if (dataSer.Items.Any(it => (it.Value < 0 | it.Value >= 0)))
                        {
                            sw.WriteLine(string.Format("#{0}", ser.Title));
                            sw.WriteLine(string.Format("#PlanName\tValue"));
                            foreach (var item in dataSer.Items)
                            {
                                sw.WriteLine(string.Format("{0}\t{1}", xaxis.Labels.ElementAt(item.CategoryIndex), item.Value));
                            }
                        }
                    }

                    else if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.HeatMapSeries().GetType()))
                    {
                        sw.WriteLine(string.Format("#{0}", ser.Title));
                        var dataSer = (OxyPlot.Series.HeatMapSeries)ser;
                        for (int i = 0; i < dataSer.Data.GetLength(0); i++)
                        {
                            for (int j = 0; j < dataSer.Data.GetLength(1); j++)
                            {
                                sw.Write(string.Format("{0}\t", dataSer.Data[i, j]));
                            }
                            sw.WriteLine();
                        }

                    }
                }
            }
        }

        // Export plotted data as csv file
        public void ExportDataAsCsv(string filePath)
        {
            using (var sw = new StreamWriter(filePath))
            {
                foreach (var ser in PlotModel.Series)
                {
                    if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.ScatterSeries().GetType()) | OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.LineSeries().GetType()) | OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.LinearBarSeries().GetType()) | OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.AreaSeries().GetType()))
                    {
                        sw.WriteLine(string.Format("#{0}", ser.Title));
                        sw.WriteLine(string.Format("#{0}\t{1}", PlotModel.Axes.FirstOrDefault(ax => ax.Position == AxisPosition.Bottom).Title, PlotModel.Axes.FirstOrDefault(ax => ax.Position == AxisPosition.Left).Title));
                        var dataSer = (OxyPlot.Series.DataPointSeries)ser;
                        foreach (var point in dataSer.Points)
                        {
                            sw.WriteLine(string.Format("{0}\t{1}", point.X.ToString(), point.Y.ToString()));
                        }
                        sw.WriteLine(string.Format("\n\n"));
                    }

                    else if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.ColumnSeries().GetType()))
                    {
                        var dataSer = (OxyPlot.Series.ColumnSeries)ser;

                        if (dataSer.Items.Any(it => (it.Value < 0 | it.Value >= 0)))
                        {
                            sw.WriteLine(string.Format("#{0}", ser.Title));
                            sw.WriteLine(string.Format("#{0}", dataSer.YAxisKey));
                            sw.WriteLine(string.Format("#CategoryIndex\tValue"));
                            foreach (var item in dataSer.Items)
                            {
                                sw.WriteLine(string.Format("{0}\t{1}", item.CategoryIndex, item.Value));
                            }
                        }
                    }

                    else if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.HeatMapSeries().GetType()))
                    {
                        sw.WriteLine(string.Format("#{0}", ser.Title));
                        var dataSer = (OxyPlot.Series.HeatMapSeries)ser;
                        for (int i = 0; i < dataSer.Data.GetLength(0); i++)
                        {
                            for (int j = 0; j < dataSer.Data.GetLength(1); j++)
                            {
                                sw.Write(string.Format("{0}\t", dataSer.Data[i, j]));
                            }
                            sw.WriteLine();
                        }

                    }
                }
            }
        }
        #endregion

        // -------------- Private methods to create plot series ------------------------------
        #region ColumnSeries

        /// <summary>
        /// Adds series with CN at three given dose levels.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void AddThreeConfSeries(PlanSetup plan, Structure structure)
        {
            // Body structure is needed here
            // If no body structure is chosen
            if (ChosenStructureId == null)
            {
                win.MessageBox.Show("Please choose a BODY structure for metric evaluation!");
                return;
            }
            // If the checked plan does not have the chosen structure in its structure set:
            else if (!plan.StructureSet.Structures.Any(str => (str.Id == ChosenStructureId & str.IsEmpty == false)))
            {
                win.MessageBox.Show("The plan is not connected to a BODY structure with the chosen ID, or the structure has no segment.");
                return;
            }
            else
            {
                Structure body = GetStructure(plan, ChosenSecondStructureId);

                var series1 = new OxyPlot.Series.ColumnSeries
                {
                    Tag = plan.Id,
                    Title = plan.Id + " " + NumericInput_1.ToString() + "Gy",
                    FillColor = OxyColors.Blue
                };

                var series2 = new OxyPlot.Series.ColumnSeries
                {
                    Tag = plan.Id,
                    Title = plan.Id + " " + NumericInput_2.ToString() + "Gy",
                    FillColor = OxyColors.BlueViolet
                };

                var series3 = new OxyPlot.Series.ColumnSeries
                {
                    Tag = plan.Id,
                    Title = plan.Id + " " + NumericInput_3.ToString() + "Gy",
                    FillColor = OxyColors.DarkGray
                };

                series1.Items.Add(new ColumnItem(Metrics.DVHMetrics.CalculateConformityIndex(plan, structure, body, NumericInput_1)));
                series2.Items.Add(new ColumnItem(Metrics.DVHMetrics.CalculateConformityIndex(plan, structure, body, NumericInput_2)));
                series3.Items.Add(new ColumnItem(Metrics.DVHMetrics.CalculateConformityIndex(plan, structure, body, NumericInput_3)));
                PlotModel.Series.Add(series1);
                PlotModel.Series.Add(series2);
                PlotModel.Series.Add(series3);
            }
        }

        /// <summary>
        /// Adds series with CDI at three given dose levels.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void AddThreeCDISeries(PlanSetup plan, Structure structure)
        {
            // Body structure is needed here
            // If no body structure is chosen
            if (ChosenStructureId == null)
            {
                win.MessageBox.Show("Please choose a BODY structure for metric evaluation!");
                return;
            }
            // If the checked plan does not have the chosen structure in its structure set:
            else if (!plan.StructureSet.Structures.Any(str => (str.Id == ChosenStructureId & str.IsEmpty == false)))
            {
                win.MessageBox.Show("The plan is not connected to a BODY structure with the chosen ID, or the structure has no segment.");
                return;
            }
            else
            {
                Structure body = GetStructure(plan, ChosenSecondStructureId);

                var series = new OxyPlot.Series.ColumnSeries
                {
                    Tag = plan.Id,
                    Title = plan.Id
                };

                // find isodose structures
                Structure iso1;
                Structure iso2;
                Structure iso3;

                if (plan.StructureSet.Structures.Any(s => (s.Id.Contains("Dose") & s.Id.Contains(NumericInput_1.ToString()))))
                {
                    iso1 = plan.StructureSet.Structures.First(s => (s.Id.Contains("Dose") & s.Id.Contains(NumericInput_1.ToString())));
                    series.Items.Add(new ColumnItem(Metrics.DVHMetrics.CalculateConformityDistanceIndex(plan, body, structure, iso1, NumericInput_1), 0));
                }
                if (plan.StructureSet.Structures.Any(s => (s.Id.Contains("Dose") & s.Id.Contains(NumericInput_2.ToString()))))
                {
                    iso2 = plan.StructureSet.Structures.First(s => (s.Id.Contains("Dose") & s.Id.Contains(NumericInput_2.ToString())));
                    series.Items.Add(new ColumnItem(Metrics.DVHMetrics.CalculateConformityDistanceIndex(plan, body, structure, iso2, NumericInput_2), 1));
                }
                if (plan.StructureSet.Structures.Any(s => (s.Id.Contains("Dose") & s.Id.Contains(NumericInput_3.ToString()))))
                {
                    iso3 = plan.StructureSet.Structures.First(s => (s.Id.Contains("Dose") & s.Id.Contains(NumericInput_3.ToString())));
                    series.Items.Add(new ColumnItem(Metrics.DVHMetrics.CalculateConformityDistanceIndex(plan, body, structure, iso3, NumericInput_3), 2));
                }

                PlotModel.Series.Add(series);
            }
        }

        /// <summary>
        /// Adds column series showing the single and combined sizes of cold spots
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void AddColdSpotSizeColumnSeries(PlanSetup plan, Structure structure)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            double cutoff = NumericInput_1;

            var coldSpots = Metrics.TextureMetrics.FindColdSpots(plan, structure, cutoff).ToList();
            coldSpots.Sort((a, b) => b.CompareTo(a));

            // create a stacked bar series
            double smallVol = 0;
            OxyPlot.Axes.CategoryAxis categoryAxis = (OxyPlot.Axes.CategoryAxis)PlotModel.Axes.FirstOrDefault(ax => ax.Key == "Plan ID");

            foreach (var cs in coldSpots)
            {
                if (cs >= 0.1)
                {
                    OxyPlot.Series.ColumnSeries series = (OxyPlot.Series.ColumnSeries)CreateStackedColumnSeries(plan, (int)PlanIndex(plan), cs, OxyColors.White, OxyColors.Black, cs.ToString());
                    series.StrokeColor = OxyColors.Black;

                    PlotModel.Series.Add(series);
                }
                else
                {
                    smallVol += cs;
                }
            }
            
            OxyPlot.Series.ColumnSeries smallSeries = (OxyPlot.Series.ColumnSeries)CreateStackedColumnSeries(plan, (int)PlanIndex(plan), smallVol, OxyColors.Gray, OxyColors.Black, "Sum of small cold spots: " + smallVol.ToString());
            smallSeries.StrokeColor = OxyColors.Black;

            categoryAxis.MajorGridlineColor = OxyColors.Transparent;
            categoryAxis.MinorGridlineColor = OxyColors.Transparent;
            PlotModel.Series.Add(smallSeries);
        }

        /// <summary>
        /// Adds column series showing the single and combined sizes of hot spots
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void AddHotSpotSizeColumnSeries(PlanSetup plan, Structure structure)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            double cutoff = NumericInput_1;

            var hotSpots = Metrics.TextureMetrics.FindHotSpots(plan, structure, cutoff).ToList();

            // sort by size
            hotSpots.Sort((a, b) => b.CompareTo(a));

            // create a stacked bar series
            double smallVol = 0;
            foreach (var cs in hotSpots)
            {
                if (cs >= 0.1)
                {
                    OxyPlot.Series.ColumnSeries series = (OxyPlot.Series.ColumnSeries)CreateStackedColumnSeries(plan, (int)PlanIndex(plan), cs, OxyColors.White, OxyColors.Black, cs.ToString());
                    series.StrokeColor = OxyColors.Black;
                    PlotModel.Series.Add(series);
                }
                else
                {
                    smallVol += cs;
                }
            }

            OxyPlot.Series.ColumnSeries smallSeries = (OxyPlot.Series.ColumnSeries)CreateStackedColumnSeries(plan, (int)PlanIndex(plan), smallVol, OxyColors.Gray, OxyColors.Black, "Sum of small cold spots: " + smallVol.ToString());
            smallSeries.StrokeColor = OxyColors.Black;
            PlotModel.Series.Add(smallSeries);
        }

        /// <summary>
        /// Creates column series showing the volume of healthy tissue traversed by each IMPT beam. Written for brain plans, but should be modifyable for other sites.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void CreateBeamPathPlot(PlanSetup plan, Structure structure)
        {
            // calculate beam path lengths through structure
            var beamPaths = Metrics.SpatialMetrics.GetBeamPathROIVolsBrain(plan, structure);

            var colors = GetCustomColors();

            int count = 0;
            foreach (var beam in plan.Beams)
            {
                if (beam.IsSetupField)
                {
                    continue;
                }

                PlotModel.Series.Add(CreateStackedColumnSeries(plan, (int)PlanIndex(plan), beamPaths[count], colors[count], OxyColors.Black, beam.Id));
                count++;
            }
        }

        /// <summary>
        /// Finds cold spots in all PlanUncertainties and adds stacked column series. Set up for 14 PUs. Adjust this method if you work with a different number.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        private void AddStackedBarColdSpotSeries(PlanSetup plan, Structure structure)
        {
            double cutoff = NumericInput_1;

            // nominal plan
            var nomCS = Metrics.TextureMetrics.FindColdSpots(plan, structure, cutoff);
            List<OxyColor> colors = new List<OxyColor> { OxyColors.LightBlue, OxyColors.MediumBlue, OxyColors.Cyan, OxyColors.DarkCyan, OxyColors.DarkBlue, OxyColors.Gray, OxyColors.LightGray, OxyColors.LightCoral, OxyColors.LightPink, OxyColors.Violet, OxyColors.Turquoise, OxyColors.Teal, OxyColors.SpringGreen };

            for (int i = 0; i < nomCS.Count(); i++)
            {
                // Give stackes areas different colors in a nice color scheme :)
                OxyColor color;
                if (i < colors.Count())
                {
                    color = colors.ElementAt(i);
                }
                else
                {
                    color = colors.ElementAt(i % colors.Count());
                }
                var series = CreateStackedColumnSeries(plan, 0, nomCS[i], color, OxyColors.Black, plan.Id);
                PlotModel.Series.Add(series); // Add to plotmodel
            }

            // if no uPlans are calculated:
            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null))
            {
                win.MessageBox.Show("Plan has no uncertainty scenarios with valid dose!");
                return;
            }
            else
            {
                foreach (var upln in plan.PlanUncertainties)
                {
                    if (upln.Dose != null)
                    {
                        plan.DoseValuePresentation = DoseValuePresentation.Absolute;
                        var uCS = Metrics.TextureMetrics.FindColdSpots(upln, structure, cutoff);
                        string ans = string.Format("{0}:\n", upln.DisplayName);
                        // Define category index
                        int catIndex;
                        switch (upln.Id)
                        {
                            case "U1":
                                catIndex = 1;
                                break;
                            case "U2":
                                catIndex = 2;
                                break;
                            case "U3":
                                catIndex = 3;
                                break;
                            case "U4":
                                catIndex = 4;
                                break;
                            case "U5":
                                catIndex = 5;
                                break;
                            case "U6":
                                catIndex = 6;
                                break;
                            case "U7":
                                catIndex = 7;
                                break;
                            case "U8":
                                catIndex = 8;
                                break;
                            case "U9":
                                catIndex = 9;
                                break;
                            case "U10":
                                catIndex = 10;
                                break;
                            case "U11":
                                catIndex = 11;
                                break;
                            case "U12":
                                catIndex = 12;
                                break;
                            case "U13":
                                catIndex = 13;
                                break;
                            case "U14":
                                catIndex = 14;
                                break;
                            default:
                                win.MessageBox.Show("Maximum possible number of planuncertainties is 14!");
                                return;
                        }

                        for (int i = 0; i < uCS.Count(); i++)
                        {
                            // Give stacked areas different colors in a nice color scheme :)
                            OxyColor color;
                            if (i < colors.Count())
                            {
                                color = colors.ElementAt(i);
                            }
                            else
                            {
                                color = colors.ElementAt(i % colors.Count());
                            }
                            var uSeries = CreateStackedColumnSeries(plan, catIndex, uCS[i], color, OxyColors.Black, upln.Id);
                            PlotModel.Series.Add(uSeries);
                            ans += uCS[i];
                            ans += string.Format("\n");
                        }
                        //win.MessageBox.Show(ans);
                    }
                }
            }
        }

        /// <summary>
        /// Finds hot spots in all PlanUncertainties and adds stacked column series. Set up for 14 PUs. Adjust this method if you work with a different number.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        private void AddStackedBarHotSpotSeries(PlanSetup plan, Structure structure)
        {
            double cutoff = NumericInput_1;

            // nominal plan
            var nomCS = Metrics.TextureMetrics.FindHotSpots(plan, structure, cutoff);
            List<OxyColor> colors = GetCustomColors();

            for (int i = 0; i < nomCS.Count(); i++)
            {
                // Give stackes areas different colors in a nice color scheme :)
                OxyColor color;
                if (i < colors.Count())
                {
                    color = colors.ElementAt(i);
                }
                else
                {
                    color = colors.ElementAt(i % colors.Count());
                }
                var series = CreateStackedColumnSeries(plan, 0, nomCS[i], color, OxyColors.Black, plan.Id);
                PlotModel.Series.Add(series); // Add to plotmodel
            }

            // if no uPlans are calculated:
            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null))
            {
                win.MessageBox.Show("Plan has no uncertainty scenarios with valid dose!");
                return;
            }
            else
            {
                foreach (var upln in plan.PlanUncertainties)
                {
                    if (upln.Dose != null)
                    {
                        plan.DoseValuePresentation = DoseValuePresentation.Absolute;
                        var uCS = Metrics.TextureMetrics.FindHotSpots(upln, structure, cutoff);
                        string ans = string.Format("{0}:\n", upln.DisplayName);
                        // Define category index
                        int catIndex;
                        switch (upln.Id)
                        {
                            case "U1":
                                catIndex = 1;
                                break;
                            case "U2":
                                catIndex = 2;
                                break;
                            case "U3":
                                catIndex = 3;
                                break;
                            case "U4":
                                catIndex = 4;
                                break;
                            case "U5":
                                catIndex = 5;
                                break;
                            case "U6":
                                catIndex = 6;
                                break;
                            case "U7":
                                catIndex = 7;
                                break;
                            case "U8":
                                catIndex = 8;
                                break;
                            case "U9":
                                catIndex = 9;
                                break;
                            case "U10":
                                catIndex = 10;
                                break;
                            case "U11":
                                catIndex = 11;
                                break;
                            case "U12":
                                catIndex = 12;
                                break;
                            case "U13":
                                catIndex = 13;
                                break;
                            case "U14":
                                catIndex = 14;
                                break;
                            default:
                                win.MessageBox.Show("Maximum possible number of planuncertainties is 14!");
                                return;
                        }

                        for (int i = 0; i < uCS.Count(); i++)
                        {
                            // Give stacked areas different colors in a nice color scheme :)
                            OxyColor color;
                            if (i < colors.Count())
                            {
                                color = colors.ElementAt(i);
                            }
                            else
                            {
                                color = colors.ElementAt(i % colors.Count());
                            }
                            var uSeries = CreateStackedColumnSeries(plan, catIndex, uCS[i], color, OxyColors.Black, upln.Id);
                            PlotModel.Series.Add(uSeries);
                            ans += uCS[i];
                            ans += string.Format("\n");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method used to create stacked column series.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="catIndex"></param>
        /// <param name="value"></param>
        /// <param name="fillColor"></param>
        /// <param name="strokeColor"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateStackedColumnSeries(PlanSetup plan, int catIndex, double value, OxyColor fillColor, OxyColor strokeColor, string title)
        {
            var series = new OxyPlot.Series.ColumnSeries();
            series.Tag = plan.Id;
            series.Title = title;
            series.StackGroup = plan.Id;
            series.IsStacked = true;
            series.StrokeColor = strokeColor;
            series.StrokeThickness = 1.5;
            series.FillColor = fillColor;
            series.ColumnWidth = 1;

            var item = new OxyPlot.Series.ColumnItem { Value = value, CategoryIndex = catIndex};
            series.Items.Add(item);

            return series;
        }
        #endregion

        #region LinearBarSeries
        /// <summary>
        /// Creates a linear bar series showing a voxelwise histogram of the maximum deviation from the nominal dose
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateMaxDeviationSeries(PlanSetup plan, Structure structure)
        {
            var series = new OxyPlot.Series.LinearBarSeries();
            var MaxDevOneD = Metrics.RobustnessMetrics.GetVoxelwiseMaximumDeviation(structure, plan);

            for (double dose = MaxDevOneD.Min(); dose < MaxDevOneD.Max(); dose += 0.1)
            {
                var voxelCount = MaxDevOneD.Count(eb => Math.Abs(eb - dose) < 0.05);
                var size = voxelCount * plan.Dose.XRes * plan.Dose.YRes * plan.Dose.ZRes / 1000;
                var point = new DataPoint(dose, size);
                series.Points.Add(point);
            }

            // Try to make fill transparent so all bars can be seen if several plans are shown. The below did not work...
            //series.FillColor = OxyColors.Transparent;
            //series.StrokeThickness = 1.5;
            //series.StrokeColor = OxyPalettes.Hot(10).Colors.ElementAt((int)PlanIndex(plan));
            series.Tag = plan.Id;
            series.Title = plan.Id;
            return series;
        }

        /// <summary>
        /// Creates a linear bar series showing a voxelwise histogram of the min-max dose span for all calculated uncertainty scenarios
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateErrorBarWidthSeries(PlanSetup plan, Structure structure)
        {
            var series = new OxyPlot.Series.LinearBarSeries();
            var ErrorBarOneD = Metrics.RobustnessMetrics.GetVoxelwiseErrorBarWidth(structure, plan);

            for (double dose = 0; dose < ErrorBarOneD.Max(); dose += 0.1)
            {
                var voxelCount = ErrorBarOneD.Count(eb => Math.Abs(eb - dose) < 0.05);
                var size = voxelCount * plan.Dose.XRes * plan.Dose.YRes * plan.Dose.ZRes / 1000;
                var point = new DataPoint(dose, size);
                series.Points.Add(point);
            }

            series.Tag = plan.Id;
            series.Title = plan.Id;
            return series;
        }

        /// <summary>
        /// Creates a linear bar series showing the differential dose gradient index for a span of doses. Isodose structures must be created in the UI before running this method.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateDifferentialGCISeries(PlanSetup plan)
        {
            // initialize series
            OxyPlot.Series.LinearBarSeries series = new OxyPlot.Series.LinearBarSeries();
            series.Tag = plan.Id;
            series.Title = plan.Id;

            series.Points.AddRange(Metrics.DVHMetrics.GetDifferentialGradientCurveDataStructure(plan));

            return series;
        }
        #endregion

        #region LineSeries

        /// <summary>
        /// Calls Metrics binary to calculate the voxelwise minimum DVH and creates a line series to plot.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateVoxelwiseMinHistogramSeries(PlanSetup plan, Structure structure)
        {
            // initialize series
            var series = new OxyPlot.Series.LineSeries();
            series.Tag = plan.Id;
            series.Title = plan.Id;

            var voxelwiseMinDVH = Metrics.RobustnessMetrics.CreateVoxelwiseMinDVH(plan, structure);

            // add point to series
            series.Points.AddRange(voxelwiseMinDVH);
            return series;
        }

        /// <summary>
        /// Creates a line series of the dose profile between two structures
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure1"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateDoseProfileLineSeries(PlanSetup plan, Structure structure1)
        {
            // -------------------------------------------------------------------------------
            // There's a bug here... pt 30534 PTV1 - PCM up throws index out of bounds error. Maybe something to do with overlapping structures? Nope. PCM up seems to be the issue...
            // -------------------------------------------------------------------------------
            Structure structure2 = plan.StructureSet.Structures.First(s => s.Id == ChosenSecondStructureId);

            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var series = new OxyPlot.Series.LineSeries();
            series.Tag = plan.Id;
            series.Title = plan.Id;

            var data = Metrics.SpatialMetrics.FindDoseOnLine(plan, structure1, structure2);
            List<double> segment = new List<double>();

            for (int i = 0; i < data.GetLength(0); i++)
            {
                DataPoint point = new DataPoint(data[i, 0], data[i, 1]);
                series.Points.Add(point);
                segment.Add(data[i, 2]);
            }

            if (segment.Any(p => p == 3)) // if the structures overlap
            {
                int overlapStartIndex = segment.IndexOf(3);
                int overlapEndIndex = overlapStartIndex + segment.Count(p => p == 3);

                PlotModel.Annotations.Add(new OxyPlot.Annotations.LineAnnotation()
                {
                    Type = LineAnnotationType.Vertical,
                    X = data[overlapStartIndex, 0],
                    Color = OxyColors.Black,
                    StrokeThickness = 3,
                    Tag = plan.Id,
                    Text = "Overlap starts"
                });
                ;

                PlotModel.Annotations.Add(new OxyPlot.Annotations.LineAnnotation()
                {
                    Type = LineAnnotationType.Vertical,
                    Color = OxyColors.Black,
                    StrokeThickness = 3,
                    Tag = plan.Id,
                    X = data[overlapEndIndex, 0],
                    Text = "Overlap ends"
                });
            }
            else
            {
                int structureOneEndIndex = segment.Count(p => p == 1);
                int structureTwoStartIndex = segment.IndexOf(2);

                PlotModel.Annotations.Add(new OxyPlot.Annotations.LineAnnotation()
                {
                    Type = LineAnnotationType.Vertical,
                    X = data[structureOneEndIndex, 0],
                    Color = OxyColors.Black,
                    StrokeThickness = 3,
                    Tag = plan.Id,
                    Text = "First structure ends"
                });

                PlotModel.Annotations.Add(new OxyPlot.Annotations.LineAnnotation()
                {
                    Type = LineAnnotationType.Vertical,
                    X = data[structureTwoStartIndex, 0],
                    Color = OxyColors.Black,
                    StrokeThickness = 3,
                    Tag = plan.Id,
                    Text = "Second structure starts"
                });
            }
            return series;
        }

        /// <summary>
        /// Creates a line series of the cold dose-location histogram (or overlap-volume histogram). 
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        private OxyPlot.Series.LineSeries CreateCumulativeColdVolumeOVH(PlanSetup plan, Structure structure)
        {
            double cutoff = NumericInput_1;

            var series = new OxyPlot.Series.LineSeries();

            var points = Metrics.SpatialMetrics.GetColdOVH(plan, structure, image, cutoff);

            if (points.Count() > 0)
            {
                series.Points.AddRange(points);
            }

            series.Tag = plan.Id;
            series.Title = plan.Id;
            return series;
        }

        /// <summary>
        /// Creates a line series of the hot dose-location histogram (or overlap-volume histogram). 
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        private OxyPlot.Series.LineSeries CreateCumulativeHotVolumeOVH(PlanSetup plan, Structure structure1, Structure structure2)
        {
            var series = new OxyPlot.Series.LineSeries();
            double cutoff = NumericInput_1;

            var points = Metrics.SpatialMetrics.GetHotOVH(plan, structure1, structure2, image, cutoff);

            if (points.Count() > 0)
            {
                series.Points.AddRange(points);
            }

            series.Tag = plan.Id;
            series.Title = plan.Id;
            return series;
        }

        /// <summary>
        /// Creates a line series of the simplified gradient index taken for a span of evaluation dose levels
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateGradientEffRadCurveSeries(PlanSetup plan)
        {
            // Body structure is needed here
            // If no body structure is chosen
            if (ChosenStructureId == null)
            {
                win.MessageBox.Show("Please choose a BODY structure for metric evaluation!");
                return null;
            }
            // If the checked plan does not have the chosen structure in its structure set:
            else if (!plan.StructureSet.Structures.Any(str => (str.Id == ChosenStructureId & str.IsEmpty == false)))
            {
                win.MessageBox.Show("The plan is not connected to a BODY structure with the chosen ID, or the structure has no segment.");
                return null;
            }
            else
            {
                Structure body = GetStructure(plan, ChosenSecondStructureId);
                var series = new OxyPlot.Series.LineSeries();
                series.Tag = plan.Id;
                series.Title = plan.Id;

                for (double d = 0.5; d < NumericInput_1; d += 0.1)
                {
                    var val = Metrics.DVHMetrics.CalculateGradientEffRadDiff(plan, body, NumericInput_1, d);
                    var point = new DataPoint(d, val);
                    series.Points.Add(point);
                }

                return series;
            }
        }

        /// <summary>
        /// Adds a line series showing the dose gradient in a selected direction.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void AddDirGradLineSeriesCC(PlanSetup plan, Structure structure)
        {
            var UpperSeries = new OxyPlot.Series.LineSeries();
            UpperSeries.Tag = plan.Id;
            UpperSeries.Title = plan.Id + "_" + Direction[0];
            UpperSeries.YAxisKey = "Position along CC axis [mm]";
            UpperSeries.Color = OxyColors.Blue;
            UpperSeries.StrokeThickness = 2;
            var LowerSeries = new OxyPlot.Series.LineSeries();
            LowerSeries.Tag = plan.Id;
            LowerSeries.Title = plan.Id + "_" + Direction[1];
            LowerSeries.YAxisKey = "Position along CC axis [mm]";
            LowerSeries.Color = OxyColors.Orange;
            LowerSeries.StrokeThickness = 2;

            var slices = GetPlanesWithStructure(plan, structure);

            for (int z = (int)slices.Min(); z <= (int)slices.Max(); z++)
            {
                if (Direction == "AP")
                {
                    double Adist = Metrics.SpatialMetrics.GetDistanceToIsodoseAlongAxis(plan, structure, NumericInput_1, Direction[0], z, NumericInput_2, NumericInput_3, NumericInput_4, NumericInput_5);
                    double Pdist = Metrics.SpatialMetrics.GetDistanceToIsodoseAlongAxis(plan, structure, NumericInput_1, Direction[1], z, NumericInput_2, NumericInput_3, NumericInput_4, NumericInput_5);

                    UpperSeries.Points.Add(new DataPoint(-Adist, (plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0));
                    LowerSeries.Points.Add(new DataPoint(Pdist, (plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0));
                }
                else if (Direction == "LR")
                {
                    double Ldist = Metrics.SpatialMetrics.GetDistanceToIsodoseAlongAxis(plan, structure, NumericInput_1, Direction[0], z, NumericInput_2, NumericInput_3, NumericInput_4, NumericInput_5);
                    double Rdist = Metrics.SpatialMetrics.GetDistanceToIsodoseAlongAxis(plan, structure, NumericInput_1, Direction[1], z, NumericInput_2, NumericInput_3, NumericInput_4, NumericInput_5);

                    UpperSeries.Points.Add(new DataPoint(-Ldist, (plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0));
                    LowerSeries.Points.Add(new DataPoint(Rdist, (plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0));
                }
            }

            PlotModel.Series.Add(UpperSeries);
            PlotModel.Series.Add(LowerSeries);
        }

        /// <summary>
        /// Adds a line series showing the directional dose gradient between a reference structure and a selected OAR
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="secondStructure"></param>
         private void AddDirGradLineSeriesCC(PlanSetup plan, Structure structure, Structure secondStructure)
         {
             var series = new OxyPlot.Series.AreaSeries();
             series.Tag = plan.Id;
             series.Title = plan.Id + "_" + Direction;
             series.YAxisKey = "Position along CC axis [mm]";

            var slices = GetPlanesWithStructure(plan, structure);

            for (int z = (int)slices.Min(); z <= (int)slices.Max(); z++)
            {
                 double dist = Metrics.SpatialMetrics.GetDistanceToIsodoseTowardPoint(plan, structure, secondStructure.CenterPoint, NumericInput_1, z, NumericInput_2, NumericInput_3, NumericInput_4, NumericInput_5);
             }
         
             PlotModel.Series.Add(series);
         }
         

        /// <summary>
        /// Create a horizontal line series
        /// </summary>
        /// <param name="value"></param>
        /// <param name="yKey"></param>
        /// <param name="xKey"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateHorizontalLine(double value, string yKey, string xKey, string tag)
        {
            var series = new OxyPlot.Series.LineSeries();
            series.YAxisKey = yKey;
            series.XAxisKey = xKey;
            series.Tag = tag;

            series.Points.Add(new DataPoint(-0.5, value));
            series.Points.Add(new DataPoint(PlansInScope.Count() + 0.5, value));

            series.Color = OxyColors.Black;

            return series;
        }

        /// <summary>
        /// Create a vertical line series.
        /// </summary>
        /// <param name="xValue"></param>
        /// <param name="maxY"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateVerticalLineSeries(double xValue, double maxY)
        {
            var series = new OxyPlot.Series.LineSeries();

            series.Points.Add(new DataPoint(xValue, 0));
            series.Points.Add(new DataPoint(xValue, maxY));

            series.Color = OxyColors.Black;

            return series;
        }

        /// <summary>
        /// Adds Area plot series to the plot model showing the spatial DVH as proposed by Zhao et al.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void CreateSpatialDVHPlot(PlanSetup plan, Structure structure)
        {
            // find crop structure
            try
            {
                Structure cropStructure = plan.StructureSet.Structures.First(s => s.Id == ChosenSecondStructureId);

                // collect margin inputs in list
                List<double> margins = new List<double> { NumericInput_1, NumericInput_2 };

                // Get DVHs
                var spatialDVHs = Metrics.SpatialMetrics.GetSpatialDvh(plan, structure, cropStructure, margins);

                // order of DVHs: near, mid, far
                var seriesNear = new OxyPlot.Series.LineSeries();
                seriesNear.Tag = plan.Id;
                seriesNear.Title = plan.Id + " near";
                foreach (var dvhPoint in spatialDVHs[0])
                {
                    var point = new DataPoint(dvhPoint.DoseValue.Dose, dvhPoint.Volume);
                    seriesNear.Points.Add(point);
                }

                var seriesMid = new OxyPlot.Series.LineSeries();
                seriesMid.Tag = plan.Id;
                seriesMid.Title = plan.Id + " mid";
                for (int i = 0; i < spatialDVHs[1].Count(); i++)
                {
                    if (i < spatialDVHs[0].Count())
                    {
                        var point = new DataPoint(spatialDVHs[1][i].DoseValue.Dose, spatialDVHs[0][i].Volume + spatialDVHs[1][i].Volume);
                        seriesMid.Points.Add(point);
                    }
                    else
                    {
                        var point = new DataPoint(spatialDVHs[1][i].DoseValue.Dose, spatialDVHs[1][i].Volume);
                        seriesMid.Points.Add(point);
                    }
                }

                var seriesFar = new OxyPlot.Series.LineSeries();
                seriesFar.Tag = plan.Id;
                seriesFar.Title = plan.Id + " far";
                for (int i = 0; i < spatialDVHs[2].Count(); i++)
                {
                    if (i < spatialDVHs[0].Count() & i < spatialDVHs[1].Count())
                    {
                        var point = new DataPoint(spatialDVHs[2][i].DoseValue.Dose, spatialDVHs[0][i].Volume + spatialDVHs[1][i].Volume + spatialDVHs[2][i].Volume);
                        seriesFar.Points.Add(point);
                    }
                    else if (i < spatialDVHs[1].Count())
                    {
                        var point = new DataPoint(spatialDVHs[2][i].DoseValue.Dose, spatialDVHs[1][i].Volume + spatialDVHs[2][i].Volume);
                        seriesFar.Points.Add(point);
                    }
                    else
                    {
                        var point = new DataPoint(spatialDVHs[2][i].DoseValue.Dose, spatialDVHs[2][i].Volume);
                        seriesFar.Points.Add(point);
                    }
                }

                PlotModel.Series.Add(seriesNear);
                PlotModel.Series.Add(seriesMid);
                PlotModel.Series.Add(seriesFar);
            }
            catch
            {
                win.MessageBox.Show("No crop structure defined or chosen structure ID cannot be found in the chosen plan structure set!");
                return;
            }
        }
        #endregion

        #region UncertaintyScatterPlotSeries
        /// <summary>
        /// Finds mean dose in all uncertainty scenarios and adds scatter series to the plotmodel.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void AddMeanDoseScatterSeries(PlanSetup plan, Structure structure)
        {
            // nominal dose
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
            double nomDose = dvh.MeanDose.Dose;

            var nomSeries = new OxyPlot.Series.ScatterSeries();
            nomSeries.Title = "Nominal";
            nomSeries.Tag = plan.Id;
            nomSeries.XAxisKey = "Linear";
            nomSeries.YAxisKey = "Dose [Gy]";
            var nominalPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), nomDose);
            nominalPoint.Tag = "Nominal";
            nomSeries.Points.Add(nominalPoint);
            nomSeries.MarkerSize = 10;
            nomSeries.MarkerFill = OxyColors.DarkRed;
            nomSeries.MarkerType = MarkerType.Circle;
            PlotModel.Series.Add(nomSeries);

            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null) | !plan.PlanUncertainties.Any()) // if the plan does not have uncertainty scenarios calculated, return only the nominal dose
            {
                win.MessageBox.Show("Plan has no calculated uncertainty scenarios!");
                return;
            }

            // create list of dose values for all uncertainty plans
            foreach (var upln in plan.PlanUncertainties)
            {
                if (upln.Dose != null)
                {
                    DVHData udvh = upln.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

                    if (udvh == null)
                    {
                        win.MessageBox.Show("Uncertainty DVHs must be calculated in order to be accessible to this script!");
                        return;
                    }

                    var tempDose = udvh.MeanDose.Dose;
                    var tempSeries = new OxyPlot.Series.ScatterSeries();
                    tempSeries.Title = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Tag = plan.Id;
                    tempSeries.XAxisKey = "Linear";
                    tempSeries.YAxisKey = "Dose [Gy]";
                    var tempPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), tempDose);
                    tempPoint.Tag = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Points.Add(tempPoint);
                    tempSeries.MarkerSize = 10;
                    tempSeries.MarkerFill = OxyColors.Black;
                    tempSeries.MarkerType = MarkerType.Circle;
                    PlotModel.Series.Add(tempSeries);
                }
            }
        }

        /// <summary>
        /// Finds D(V) in all uncertainty scenarios and adds scatter series to the plotmodel.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void AddDxDoseScatterSeries(PlanSetup plan, Structure structure, bool volIsAbs)
        {

            double nomDose = 0;
            double volume = NumericInput_1;

            // nominal dose
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

            if (volIsAbs)
                nomDose = Metrics.DVHMetrics.DoseFromDVH(dvh, volume);
            else
                nomDose = Metrics.DVHMetrics.DoseFromDVH(dvh, volume * structure.Volume / 100);

            var nomSeries = new OxyPlot.Series.ScatterSeries();
            nomSeries.Title = "Nominal";
            nomSeries.Tag = plan.Id;
            var nominalPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), nomDose);
            nominalPoint.Tag = "Nominal";
            nomSeries.XAxisKey = "Linear";
            nomSeries.YAxisKey = "Dose [Gy]";
            nomSeries.Points.Add(nominalPoint);
            nomSeries.MarkerSize = 10;
            nomSeries.MarkerFill = OxyColors.DarkRed;
            nomSeries.MarkerType = MarkerType.Circle;
            PlotModel.Series.Add(nomSeries);

            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null)) // if the plan does not have uncertainty scenarios calculated, return only the nominal dose
            {
                win.MessageBox.Show("Plan has no calculated uncertainty scenarios!");
                return;
            }

            // create list of dose values for all uncertainty plans
            foreach (var upln in plan.PlanUncertainties)
            {
                if (upln.Dose != null)
                {
                    double tempDose = 0;
                    DVHData udvh = upln.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    if (udvh == null)
                    {
                        win.MessageBox.Show("Uncertainty DVHs must be calculated in order to be accessible to this script!");
                        return;
                    }

                    if (volIsAbs)
                        tempDose = Metrics.DVHMetrics.DoseFromDVH(udvh, volume);
                    else
                        tempDose = Metrics.DVHMetrics.DoseFromDVH(udvh, volume * structure.Volume / 100);

                    var tempSeries = new OxyPlot.Series.ScatterSeries();
                    tempSeries.Title = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Tag = plan.Id;
                    tempSeries.XAxisKey = "Linear";
                    tempSeries.YAxisKey = "Dose [Gy]";
                    var tempPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), tempDose);
                    tempPoint.Tag = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Points.Add(tempPoint);
                    tempSeries.MarkerSize = 10;
                    tempSeries.MarkerFill = OxyColors.Black;
                    tempSeries.MarkerType = MarkerType.Circle;
                    PlotModel.Series.Add(tempSeries);
                }
            }
        }

        /// <summary>
        /// Finds V(D) in all uncertainty scenarios and adds scatter series to the plotmodel.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void AddVxScatterSeries(PlanSetup plan, Structure structure, bool isAbs)
        {

            double nomVol = 0;
            double dose = NumericInput_1;

            // nominal dose
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

            if (isAbs)
                nomVol = Metrics.DVHMetrics.VolumeFromDVH(dvh, dose);
            else
                nomVol = Metrics.DVHMetrics.VolumeFromDVH(dvh, dose * plan.TotalPrescribedDose.Dose / 100);

            var nomSeries = new OxyPlot.Series.ScatterSeries();
            nomSeries.Title = "Nominal";
            nomSeries.Tag = plan.Id;
            nomSeries.XAxisKey = "Linear";
            nomSeries.YAxisKey = "Volume [cc]";
            var nominalPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), nomVol);
            nominalPoint.Tag = "Nominal";
            nomSeries.Points.Add(nominalPoint);
            nomSeries.MarkerSize = 10;
            nomSeries.MarkerFill = OxyColors.DarkRed;
            nomSeries.MarkerType = MarkerType.Circle;
            PlotModel.Series.Add(nomSeries);

            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null)) // if the plan does not have uncertainty scenarios calculated, return only the nominal dose
            {
                win.MessageBox.Show("Plan has no calculated uncertainty scenarios!");
                return;
            }

            // create list of dose values for all uncertainty plans
            foreach (var upln in plan.PlanUncertainties)
            {
                if (upln.Dose != null)
                {
                    double tempVol = 0;
                    DVHData udvh = upln.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    if (udvh == null)
                    {
                        win.MessageBox.Show("Uncertainty DVHs must be calculated in order to be accessible to this script!");
                        return;
                    }

                    if (isAbs)
                        tempVol = Metrics.DVHMetrics.VolumeFromDVH(udvh, dose);
                    else
                        tempVol = Metrics.DVHMetrics.VolumeFromDVH(udvh, dose * plan.TotalPrescribedDose.Dose / 100);

                    var tempPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), tempVol);
                    var tempSeries = new OxyPlot.Series.ScatterSeries();
                    tempSeries.Title = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Tag = plan.Id;
                    tempSeries.XAxisKey = "Linear";
                    tempSeries.YAxisKey = "Volume [cc]";
                    tempPoint.Tag = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Points.Add(tempPoint);
                    tempSeries.MarkerSize = 10;
                    tempSeries.MarkerFill = OxyColors.Black;
                    tempSeries.MarkerType = MarkerType.Circle;
                    PlotModel.Series.Add(tempSeries);
                }
            }
        }
        #endregion

        #region ScatterSeries

        /// <summary>
        /// Calls the Metrics binary to calculate the Conformity Number as proposed e.g. by van't Riet et al. and returns a plot series.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateCIScatterSeries(PlanSetup plan, Structure structure)
        {
            // Body structure is needed here
            // If no body structure is chosen
            if (ChosenStructureId == null)
            {
                win.MessageBox.Show("Please choose a BODY structure for metric evaluation!");
                return null;
            }
            // If the checked plan does not have the chosen structure in its structure set:
            else if (!plan.StructureSet.Structures.Any(str => (str.Id == ChosenStructureId & str.IsEmpty == false)))
            {
                win.MessageBox.Show("The plan is not connected to a BODY structure with the chosen ID, or the structure has no segment.");
                return null;
            }
            else
            {
                Structure body = GetStructure(plan, ChosenSecondStructureId);

                // Initiate series and set title and tag
                var series = new OxyPlot.Series.ScatterSeries();
                series.Title = plan.Id;
                series.Tag = plan.Id;
                series.YAxisKey = "CI";
                series.XAxisKey = "Linear";
                series.MarkerType = MarkerType.Circle;
                series.MarkerFill = OxyColors.Black;
                series.MarkerSize = 10;

                // calculate DVH
                DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

                // Calculate CI. The dose level is given by the user
                double value = Metrics.DVHMetrics.CalculateConformityIndex(plan, structure, body, NumericInput_1);
                //double value = 5;

                // Create column item and add to series
                series.Points.Add(new ScatterPoint(PlanIndex(plan), value));

                return series;
            }
        }

        /// <summary>
        /// Calls the Metrics binary to calculate the Homogeneity Index as proposed ICRU reports 64 and 83 and returns a plot series.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateHIScatterSeries(PlanSetup plan, Structure structure)
        {
            // Initiate series and set title and tag
            var series = new OxyPlot.Series.ScatterSeries();
            series.Title = plan.Id;
            series.Tag = plan.Id;
            series.YAxisKey = "HI";
            series.XAxisKey = "Linear";
            series.MarkerType = MarkerType.Circle;
            series.MarkerFill = OxyColors.Black;
            series.MarkerSize = 10;

            // calculate DVH
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

            // Calculate HI
            double value = Metrics.DVHMetrics.CalculateHI_ICRU(plan, structure);

            // Create scatterpoint item and add to series
            series.Points.Add(new ScatterPoint(PlanIndex(plan), value));

            return series;
        }

        /// <summary>
        /// Calls the Metrics binary to calculate the Gradient Index defined as the difference between the effective radii of the chosen reference isodose and its 50% isodose. Returns an Oxyplot plot series.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateGIReffScatterSeries(PlanSetup plan)
        {
            // Body structure is needed here
            // If no body structure is chosen
            if (ChosenStructureId == null)
            {
                win.MessageBox.Show("Please choose a BODY structure for metric evaluation!");
                return null;
            }
            // If the checked plan does not have the chosen structure in its structure set:
            else if (!plan.StructureSet.Structures.Any(str => (str.Id == ChosenStructureId & str.IsEmpty == false)))
            {
                win.MessageBox.Show("The plan is not connected to a BODY structure with the chosen ID, or the structure has no segment.");
                return null;
            }
            else
            {
                Structure body = GetStructure(plan, ChosenSecondStructureId);

                // Initiate series and set title and tag
                var series = new OxyPlot.Series.ScatterSeries();
                series.Title = plan.Id;
                series.Tag = plan.Id;
                series.YAxisKey = "GI";
                series.XAxisKey = "Linear";
                series.MarkerType = MarkerType.Circle;
                series.MarkerFill = OxyColors.Black;
                series.MarkerSize = 10;

                // Calculate GI. The reference dose level is given by the user
                double value = Metrics.DVHMetrics.CalculateGradientEffRadDiff(plan, body, NumericInput_1, 0.5 * NumericInput_1);

                // Create column item and add to series
                series.Points.Add(new ScatterPoint(PlanIndex(plan), value));

                return series;
            }
        }

        /// <summary>
        /// Calls the Metrics binary and adds plot series showing min and max distances from each cold spot to the target's edge.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void CreateColdSpotLocationScatterSeries(PlanSetup plan, Structure structure)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            // define color scheme
            List<OxyColor> colors = GetCustomColors();
            OxyColor color;

            if (PlansInScope.Count() <= colors.Count())
            {
                color = colors[(int)PlanIndex(plan)];
            }
            else
            {
                color = colors[(int)PlanIndex(plan) % colors.Count()];
            }

            // initialize series
            var seriesNear = new OxyPlot.Series.ScatterSeries();
            seriesNear.Title = plan.Id + " closest";
            seriesNear.Tag = plan.Id;
            seriesNear.MarkerType = MarkerType.Circle;
            seriesNear.MarkerSize = 8;
            seriesNear.MarkerFill = color;

            var seriesFar = new OxyPlot.Series.ScatterSeries();
            seriesFar.Title = plan.Id + " farthest";
            seriesFar.Tag = plan.Id;
            seriesFar.MarkerType = MarkerType.Diamond;
            seriesFar.MarkerSize = 8;
            seriesFar.MarkerFill = color;

            var seriesPerc = new OxyPlot.Series.ScatterSeries();
            seriesPerc.Title = plan.Id + " 5-95%";
            seriesPerc.Tag = plan.Id;
            seriesPerc.MarkerType = MarkerType.Cross;
            seriesPerc.MarkerSize = 6;
            seriesPerc.MarkerStroke = color;

            

            var distances = Metrics.SpatialMetrics.FindCSNearFarDistance(plan, structure, NumericInput_1);

            foreach (var cs in distances)
            {
                if (cs.Item1 >= 0.03)
                {
                    seriesNear.Points.Add(new OxyPlot.Series.ScatterPoint(cs.Item2[0], cs.Item1));
                    seriesFar.Points.Add(new OxyPlot.Series.ScatterPoint(cs.Item2[1], cs.Item1));
                    seriesPerc.Points.Add(new OxyPlot.Series.ScatterPoint(cs.Item2[2], cs.Item1));
                    seriesPerc.Points.Add(new OxyPlot.Series.ScatterPoint(cs.Item2[3], cs.Item1));

                    var seriesLine = new OxyPlot.Series.LineSeries();
                    seriesLine.Title = "";
                    seriesLine.Tag = plan.Id;
                    seriesLine.Color = color;
                    seriesLine.StrokeThickness = 2;
                    seriesLine.Points.Add(new DataPoint(cs.Item2[0], cs.Item1));
                    seriesLine.Points.Add(new DataPoint(cs.Item2[1], cs.Item1));

                    PlotModel.Series.Add(seriesLine);
                }
            }

            PlotModel.Series.Add(seriesNear);
            PlotModel.Series.Add(seriesFar);
            PlotModel.Series.Add(seriesPerc);
        }

        /// <summary>
        /// Calls the Metrics binary and adds plot series showing min and max distances from each hot spot to the target's edge.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void CreateHotSpotLocationScatterSeries(PlanSetup plan, Structure structure)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;
            Structure target = plan.StructureSet.Structures.FirstOrDefault(ss => ss.Id == ChosenSecondStructureId);

            // define color scheme
            List<OxyColor> colors = GetCustomColors();
            OxyColor color;

            if (PlansInScope.Count() <= colors.Count())
            {
                color = colors[(int)PlanIndex(plan)];
            }
            else
            {
                color = colors[(int)PlanIndex(plan) % colors.Count()];
            }

            // initialize series
            var seriesNear = new OxyPlot.Series.ScatterSeries();
            seriesNear.Title = plan.Id + " closest";
            seriesNear.Tag = plan.Id;
            seriesNear.MarkerType = MarkerType.Circle;
            seriesNear.MarkerSize = 8;
            seriesNear.MarkerFill = color;

            var seriesFar = new OxyPlot.Series.ScatterSeries();
            seriesFar.Title = plan.Id + " farthest";
            seriesFar.Tag = plan.Id;
            seriesFar.MarkerType = MarkerType.Diamond;
            seriesFar.MarkerSize = 8;
            seriesFar.MarkerFill = color;

            var seriesPerc = new OxyPlot.Series.ScatterSeries();
            seriesPerc.Title = plan.Id + " 5-95%";
            seriesPerc.Tag = plan.Id;
            seriesPerc.MarkerType = MarkerType.Cross;
            seriesPerc.MarkerSize = 6;
            seriesPerc.MarkerStroke = color;

            var seriesLine = new OxyPlot.Series.LineSeries();
            seriesLine.Title = "";
            seriesLine.Tag = plan.Id;
            seriesLine.Color = color;
            seriesLine.StrokeThickness = 1.5;

            var distances = Metrics.SpatialMetrics.FindHSNearFarDistance(plan, target, structure, NumericInput_1);

            foreach (var cs in distances)
            {
                seriesNear.Points.Add(new OxyPlot.Series.ScatterPoint(cs.Item2[0], cs.Item1));
                win.MessageBox.Show(string.Format("{0}, {1}\n", cs.Item2[0], cs.Item1));
                seriesFar.Points.Add(new OxyPlot.Series.ScatterPoint(cs.Item2[1], cs.Item1));
                seriesPerc.Points.Add(new OxyPlot.Series.ScatterPoint(cs.Item2[2], cs.Item1));
                seriesPerc.Points.Add(new OxyPlot.Series.ScatterPoint(cs.Item2[3], cs.Item1));
                seriesLine.Points.Add(new DataPoint(cs.Item2[0], cs.Item1));
                seriesLine.Points.Add(new DataPoint(cs.Item2[1], cs.Item1));
            }

            PlotModel.Series.Add(seriesNear);
            PlotModel.Series.Add(seriesFar);
            PlotModel.Series.Add(seriesPerc);
            PlotModel.Series.Add(seriesLine);
        }

        /// <summary>
        /// Calls the Metrics binary and calculates dose gradient towards an OAR defined as the distance between the OAR and isodose. Isodose structure must be generated in the UI previously to running.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="OAR"></param>
        private void AddOARGradientSeries(PlanSetup plan, Structure OAR)
        {
            var distances = Metrics.SpatialMetrics.GetOARGradientCurvePoints(plan, OAR); 

            // List of colors
            List<OxyColor> colors = GetCustomColors();
            OxyColor color = colors[(int)PlanIndex(plan) % colors.Count()];

            // initialize series
            OxyPlot.Series.ScatterSeries absSeries = new OxyPlot.Series.ScatterSeries();
            absSeries.Tag = plan.Id;
            absSeries.Title = plan.Id;
            absSeries.MarkerFill = color;
            absSeries.MarkerType = MarkerType.Diamond;
            absSeries.MarkerSize = 12;

            OxyPlot.Series.ScatterSeries percSeries = new OxyPlot.Series.ScatterSeries();
            percSeries.Tag = plan.Id;
            percSeries.Title = plan.Id;
            percSeries.MarkerType = MarkerType.Cross;
            percSeries.MarkerStroke = color;
            percSeries.MarkerStrokeThickness = 2;
            percSeries.MarkerSize = 12;

            // Add data points to series
            absSeries.Points.AddRange(distances.Item1);
            percSeries.Points.AddRange(distances.Item2);

            // Add series to plot model
            PlotModel.Series.Add(absSeries);
            PlotModel.Series.Add(percSeries);
        }

        /// <summary>
        /// Adds a scatter series showing the dose gradient in a selected direction.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        private void AddDirGradScatterSeriesCCAvg(PlanSetup plan, Structure structure)
        {
            var UpperSeries = new OxyPlot.Series.ScatterSeries();
            UpperSeries.Tag = plan.Id;
            UpperSeries.Title = plan.Id + "_" + Direction[0];
            UpperSeries.YAxisKey = "Distance to isodose [mm]";
            var LowerSeries = new OxyPlot.Series.ScatterSeries();
            LowerSeries.Tag = plan.Id;
            LowerSeries.Title = plan.Id + "_" + Direction[1];
            LowerSeries.YAxisKey = "Distance to isodose [mm]";

            // get number of slices with structure delineated
            List<int> slices = GetPlanesWithStructure(plan, structure);
            int noSlices = slices.Count();

            for (double p = 1; p > 0.01; p -= 0.2) // p > 0.01 to avoid near-zero dose calculations (due to floating point errors?)
            {
                // initiate variables to calculate average over CC slices
                double upperDist = 0;
                double lowerDist = 0;

                for (int z = slices.Min(); z <= slices.Max(); z++)
                {
                    // add distance on current slice
                    upperDist += Metrics.SpatialMetrics.GetDistanceToIsodoseAlongAxis(plan, structure, NumericInput_1 * p, Direction[0], z, NumericInput_2, NumericInput_3, NumericInput_4, NumericInput_5);
                    lowerDist += Metrics.SpatialMetrics.GetDistanceToIsodoseAlongAxis(plan, structure, NumericInput_1 * p, Direction[1], z, NumericInput_2, NumericInput_3, NumericInput_4, NumericInput_5);
                }

                UpperSeries.Points.Add(new ScatterPoint(p * NumericInput_1, upperDist / noSlices));
                LowerSeries.Points.Add(new ScatterPoint(p * NumericInput_1, lowerDist / noSlices));
            }

            PlotModel.Series.Add(UpperSeries);
            PlotModel.Series.Add(LowerSeries);
        }

        /// <summary>
        /// Adds a scatter series showing the directional dose gradient between a reference structure and a selected OAR
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="secondStructure"></param>
        private void AddDirGradScatterSeriesCCAvg(PlanSetup plan, Structure structure, Structure secondStructure)
        {
            var series = new OxyPlot.Series.ScatterSeries();
            series.Tag = plan.Id;
            series.Title = plan.Id;
            series.YAxisKey = "Distance to isodose [mm]";

            double dist = 0;
            // get number of slices with structure delineated
            List<int> slices = GetPlanesWithStructure(plan, structure);
            int noSlices = slices.Count();

            for (double p = 1; p > 0; p -= 0.2)
            {
                for (int z = slices.Min(); z <= slices.Max(); z++)
                {
                    dist += Metrics.SpatialMetrics.GetDistanceToIsodoseTowardPoint(plan, structure, secondStructure.CenterPoint, p * NumericInput_1, z, NumericInput_2, NumericInput_3, NumericInput_4, NumericInput_5);
                }

                series.Points.Add(new ScatterPoint(p * NumericInput_1, dist / noSlices));
            }

            PlotModel.Series.Add(series);
        }

        /// <summary>
        /// Creates a scatter series of the GCI for a number of dose levels. Isodose structures mus be created in the UI prior.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="refStructure"></param>
        /// <returns></returns>
        private OxyPlot.Series.Series CreateCumulativeGradientCurveSeries(PlanSetup plan, Structure refStructure)
        {
            var series = new OxyPlot.Series.LineSeries();
            series.Tag = plan.Id;
            series.Title = plan.Id;
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var points = Metrics.DVHMetrics.GetCumulativeGradientCurveData(plan, refStructure);

            series.Points.AddRange(points);
            return series;
        }

        #endregion

        #region HeatMapPlots

        /// <summary>
        /// Method that creates a heat map plot of the dose at a certain given distance from a structure
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="distance"></param>
        private void CreateDoseAtDistanceHeatMap(PlanSetup plan, Structure structure)
        {
            double distance = NumericInput_1;

            var series = new OxyPlot.Series.HeatMapSeries();
            series.Tag = plan.Id;
            series.Title = plan.Id;
            series.X0 = 0;
            series.X1 = 359;
            series.Y0 = 0;
            series.Y1 = 179;
            series.Data = Metrics.SpatialMetrics.CalculateDoseAtDistance(plan, structure, distance);

            PlotModel.Series.Add(series);
        }

        private void CreateDistanceToDoseHeatMap(PlanSetup plan, Structure structure, double dose)
        {
            var series = new OxyPlot.Series.HeatMapSeries();
            series.Tag = plan.Id;
            series.Title = plan.Id;
            series.X0 = 0;
            series.X1 = 359;
            series.Y0 = 0;
            series.Y1 = 179;
            //series.Data = Metrics.SpatialMetrics.CalculateDistanceToDose(plan, structure, dose);

            PlotModel.Series.Add(series);
        }
        #endregion

        #region HelperMethods

        /// <summary>
        /// Method to find structure in a plan's structure set with the given string ID
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private Structure GetStructure(PlanSetup plan, string id)
        {
            try
            {
                return plan.StructureSet.Structures.FirstOrDefault(s => s.Id == id);
            }
            catch
            {
                win.MessageBox.Show("Chosen structure could not be found in the plan's structure set. Showing first structure in plan's set");
                return plan.StructureSet.Structures.First();
            }
        }

        private double GetNumberOfUplns(IEnumerable<PlanSetup> plansInScope)
        {
            double maxNumber = 0;
            double number;

            foreach (PlanSetup plan in plansInScope)
            {
                number = 0;
                foreach (PlanUncertainty upln in plan.PlanUncertainties)
                {
                    if (upln.Dose != null)
                    {
                        number++;
                    }
                }

                if (number > maxNumber)
                {
                    maxNumber = number;
                }
            }

            return maxNumber;
        }

        private List<OxyColor> GetCustomColors()
        {
            return new List<OxyColor> { OxyColors.DarkBlue, OxyColors.DarkGray, OxyColors.Cyan, OxyColors.Purple, OxyColors.Magenta, OxyColors.Orange, OxyColors.Red, OxyColors.RoyalBlue };
        }

        private List<int> GetPlanesWithStructure(PlanSetup plan, Structure structure)
        {
            List<int> retList = new List<int>();

            // Loop over planes
            for (int z = 0; z < plan.Dose.ZSize; z++)
            {
                if (structure.GetContoursOnImagePlane(z).Count() > 0)
                {
                    retList.Add(z);
                }
            }

            return retList;
        }

        // method creates a list of empty strings for category axes with no labels
        private List<string> EmptyStrings(int length)
        {
            var list = new List<string>();
            for (int i = 0; i < length; i++)
            {
                list.Add("");
            }

            return list;
        }
        #endregion
    }
}
