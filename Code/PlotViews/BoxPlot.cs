﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Axes;

namespace PlanComparisonV13.PlotViews
{
    public class BoxPlot : IPlotView
    {
        public void SetAxes(PlotModel plotModel, double xMin, double xMax, double yMin, double yMax, string xLabel, string[] yLabel, IEnumerable<string> xTickLabels)
        {
            // Axes
            //y axis
            plotModel.Axes.Add(new LinearAxis
            {
                Title = yLabel[0],
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left
            });

            // plotting boxplots requires linear x-axis
            plotModel.Axes.Add(new LinearAxis
            {
                MajorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Bottom,
                MajorStep = 1,
                Minimum = xMin,
                Maximum = xMax,
                IsAxisVisible = false // do not show linear x axis
            });

            // Category axis to show plan IDs instead of indices
            var catxaxis = new CategoryAxis
            {
                Title = xLabel,
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Bottom,
                MajorStep = 1,
                IsAxisVisible = true,
                Angle = 90,
                Key = xLabel
            };


            catxaxis.Labels.AddRange(xTickLabels);
            plotModel.Axes.Add(catxaxis);
        }

        public void SetTitle(PlotModel plotModel, string title)
        {
            // Title
            plotModel.Title = title;
        }

        public void SetLegend(PlotModel plotModel, bool isLegendVisible, OxyPlot.LegendPlacement placement, OxyPlot.LegendPosition position, OxyPlot.LegendOrientation orientation)
        {
            // legend
            plotModel.LegendPlacement = placement;
            plotModel.LegendBorder = OxyColors.Black;
            plotModel.LegendBackground = OxyColor.FromAColor(32, OxyColors.Black);
            plotModel.LegendPosition = position;
            plotModel.LegendOrientation = orientation;
            plotModel.IsLegendVisible = isLegendVisible;
        }
    }
}
