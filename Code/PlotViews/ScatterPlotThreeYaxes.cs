﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OxyPlot;
using OxyPlot.Axes;
using VMS.TPS.Common.Model.Types;
using VMS.TPS.Common.Model.API;

namespace PlanComparisonV13.PlotViews
{
    public class ScatterPlotThreeYaxes : IPlotView
    {
        public void SetAxes(PlotModel plotModel, double xMin, double xMax, double yMin, double yMax, string xLabel, string[] yLabel, IEnumerable<string> xTickLabels)
        {
            var xAxisLin = new LinearAxis
            {
                Title = xLabel,
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                MajorGridlineStyle = LineStyle.Solid,
                AxisTitleDistance = 15,
                Position = AxisPosition.Bottom,
                Angle = 90,
                Minimum = xMin,
                Maximum = xMax,
                Key = "Linear",
                IsAxisVisible = false
            };

            var xAxisCat = new CategoryAxis
            {
                Title = xLabel,
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                MajorGridlineStyle = LineStyle.Solid,
                AxisTitleDistance = 15,
                Position = AxisPosition.Bottom,
                Angle = 90,
                Key = xLabel
            };

            xAxisCat.Labels.AddRange(xTickLabels);

            plotModel.Axes.Add(xAxisLin);
            plotModel.Axes.Add(xAxisCat);

            // Three y-axes
            var yAxis1 = new LinearAxis
            {
                Title = yLabel[0],
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                StartPosition = 0,
                EndPosition = 0.3,
                Key = yLabel[0]
            };

            plotModel.Axes.Add(yAxis1);

            var yAxis2 = new LinearAxis
            {
                Title = yLabel[1],
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                StartPosition = 0.35,
                EndPosition = 0.65,
                Key = yLabel[1]
            };

            plotModel.Axes.Add(yAxis2);

            var yAxis3 = new LinearAxis
            {
                Title = yLabel[2],
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                StartPosition = 0.7,
                EndPosition = 1,
                Key = yLabel[2]
            };

            plotModel.Axes.Add(yAxis3);
        }

        public void SetLegend(PlotModel plotModel, bool isLegendVisible, LegendPlacement placement, LegendPosition position, LegendOrientation orientation)
        {
            //legend
            plotModel.LegendPlacement = placement;
            plotModel.LegendBorder = OxyColors.Black;
            plotModel.LegendBackground = OxyColor.FromAColor(32, OxyColors.Black);
            plotModel.LegendPosition = position;
            plotModel.LegendOrientation = orientation;
            plotModel.IsLegendVisible = isLegendVisible;
        }

        public void SetTitle(PlotModel plotModel, string title)
        {
            // Title
            plotModel.Title = title;
        }
    }
}
