﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot.Wpf;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using OxyPlot.Annotations;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;


namespace VMS.TPS
{
    public class Script
    {
        public Script()
        {
        }

        // dummy objects to force load of OxyPlot packages
        private static readonly PlotModel plotModel = new PlotModel();
        private static readonly PlotView plotView = new PlotView();
        private static readonly ColumnItem columnItem = new ColumnItem();
        private static readonly OxyPlot.Axes.CategoryAxis ax = new OxyPlot.Axes.CategoryAxis();
        private static readonly OxyPlot.Annotations.LineAnnotation line = new OxyPlot.Annotations.LineAnnotation();

        public void Execute(ScriptContext context, System.Windows.Window window)
        {
            var mainViewModel = new PlanComparisonV13.MainViewModel(context);
            var mainView = new PlanComparisonV13.MainView(mainViewModel);
            window.Title = "Plan Evaluation";
            window.Content = mainView;
        }
    }
}

